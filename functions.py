import sys

# open and format token
def get_token(filename='token.txt', out=sys.stdout):
    out.write('using token path {}\n'.format(filename))
    with open(filename) as f:
        token = f.readline()
    out.write("- read token with {} chars\n".format(len(token.strip())))
    return token.strip()
    print (token)
